##############################################
# REQUIREMENTS
# +Ubuntu,Centos - Docker, Docker Compose
# +OSX, Windows - Docker, DockerTools (Docker Compose, Docker Machine)


# START WORK
---
for MacOS and Windows go to docker-machine or use copy git content
$ docker-machine ssh
$ git clone https://antonioiksi@bitbucket.org/completecode/docker-drupal.git
// alternative for MacOS and Windows
//$ git clone rep.git
//$ docker-machine ip
//copy with scp login:docker pwd:tcuser
//$ scp -r docker-drupal docker@192.168.99.100:/home/docker

for Ubintu clone this content to some folder
$ git clone https://antonioiksi@bitbucket.org/completecode/docker-drupal.git

go to docker-drupal
$ cd docker-drupal

install docker-compose
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.8.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose

up the containers
$ docker-compose up -d

get containers ID
$ docker ps

connect with bash to WEB docker
$ docker exec -it dockerdrupalozon_web_1 bash

you must be in /var/www/html
define site folder for ex: drupal1 or drupal2 etc (below: drupal1)

download drupal
$ composer create-project drupal/drupal drupal1 8.2.3

define IP for MYSQL server
$ docker inspect dockerdrupalozon_db_1 | grep IP

in previous step we defined IP for mysql server: 172.21.0.2 then install dupal
$ drush site-install standard --account-name=admin --account-pass=admin --db-url=mysql://root:drupal@172.21.0.2/drupal1

change rights for folder drupal1
$ chmod -R 777 ../drupal1

$ drush cr

TEST IT localhost:8000/drupal1

##############################################
# BACKUP SITE
go to site folder
$ cd drupal1

backup site
$ drush archive-backup --destination=../backup-drupal1.tar.gz

##############################################
#RESTORE SITE
go to /var/www/html and exec restore drush
check folder druapl1
$ rm -R drupal1

$ drush archive-restore backup-drupal1.tar.gz

change right to the folder druapl1
$ chmod -R 777 drupal1

go to drupal1 and clear cash
$ cd drupal1
$ drush cr

##############################################
#     created by mrozon and antonioiksi      #
##############################################
